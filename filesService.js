const fs = require('fs');
const path = require("path");
const fsp = require('fs/promises');
const pathFile = path.resolve('./files');
const extsPath = ['txt', 'json', 'xml', 'js', 'log', 'yaml'];
let ArrFiles = [];


function createFile(req, res) {
    try {


        if (req.body.filename === undefined) {
            res.status(400).json({"message": 'Write filename'});
            return;
        }

        if (req.body.content === undefined) {
            res.status(400).json({"message": 'Write  content'});
            return;
        }


        if (extsPath.includes(req.body.filename.split('.').pop())) {
            fs.writeFileSync(path.join(pathFile, req.body.filename), req.body.content, {flag: 'a+'});
            res.status(200).send({"message": "File created successfully"});
        } else {
            res.status(400).send({
                "message": `Wrong ext file`
            });
        }
    } catch (error) {
        res.status(400).json({"message": 'Server error'});
    }

}

//getFiles
function getFiles(req, res) {
    fs.readdir(pathFile, (err, data) => {
        if (err) {
            console.log(err);
            return;
        }
        data.forEach(file => {
            ArrFiles.push(file);
            return ArrFiles;
        })
        res.status(200).send({
            "message": "Success",
            "files": [...new Set(ArrFiles)]
        });
    })
}

const getFile = async (req, res) => {
    try {
        let createDate;

        fs.stat(pathFile + req.url, (error, stats) => {
            if (error) {
                console.log(error);
                return;
            }
            createDate = stats.birthtime;
        });
        res.status(200).send({
            "message": "Success",
            "filename": path.basename(req.url),
            "content": await fsp.readFile(pathFile + req.url, {encoding: 'utf8'}),
            "extension": path.extname(req.url).split('.').pop(),
            "uploadedDate": createDate
        });
    } catch (error) {
        res.status(400).json({"message": "Server error"});
    }
}

const editFile = (req, res) => {
    try {

        fs.writeFile(path.join(pathFile, req.body.filename), req.body.content, 'utf-8', err => {
            if (err) {
                console.log(err);
                return;
            }
            res.status(200).send({
                "message": "File was edited successfully",
            })
        });
    } catch (error) {
        res.status(400).json({"message": "Server error"});
    }
}

const deleteFile = (req, res) => {
    try {

        fs.unlink(path.join(pathFile, req.body.filename), err => {
            if (err) throw err;
        })
        res.status(200).send({
            "message": "File was deleted successfully",
        });
    } catch (error) {
        res.status(400).json({"message": "Server error"});
    }
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    editFile,
    deleteFile
}
