const express = require('express');
const router = express.Router();
const {createFile, getFiles, getFile, editFile, deleteFile} = require('./filesService.js');

router.post('/', createFile);

router.get('/:filename', getFile);

router.get('/', getFiles);

router.patch('/:filename', editFile);

router.delete('/:filename', deleteFile);


module.exports = {
    filesRouter: router
};
